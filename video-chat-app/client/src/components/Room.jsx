
import React, {useEffect, useRef} from 'react'
import RecordView from "./Recording";
import { useScreenshot, createFileName } from "use-react-screenshot";

export const Room = (props) => {
  const userVideo = useRef()
  const userStream = useRef()
  const partnerVideo = useRef()
  const peerRef = useRef()
  const webSocketRef = useRef()

  const [image, takeScreenShot] = useScreenshot({
    type: "image/jpeg",
    quality: 1.0
  });

  const download = (image, { name = "img", extension = "jpg" } = {}) => {
    const a = document.createElement("a");
    a.href = image;
    a.download = createFileName(extension, name);
    a.click();
  };

  const downloadScreenshot = () => takeScreenShot(userVideo.current).then(download);
  
  const openCamera = async () => {
    const allDevices = await navigator.mediaDevices.enumerateDevices()
    const cameras = allDevices.filter(
        (device) => device.kind == "videoinput"
    );

    const constraints = {
        audio: true,
        video: {
            deviceId: cameras[0].deviceId,
        },
    }
    
    try {
        return await navigator.mediaDevices.getUserMedia(constraints)
    } catch (error) {
        console.log(error)
    }
  }

  useEffect(() => {
    openCamera().then((stream)=> {
        userVideo.current.srcObject = stream
        userStream.current = stream

        webSocketRef.current = new WebSocket(
            `ws://localhost:8080/join?roomID=${props.match.params.roomID}`
        );

        webSocketRef.current.addEventListener("open", () => {
            webSocketRef.current.send(JSON.stringify({join:true}));
        })

        webSocketRef.current.addEventListener("message", async (e) => {
            const message = JSON.parse(e.data)

            if(message.join) {
                callUser();
            }

            if(message.offer){
                handleOffer(message.offer)
            }

            if(message.answer){
                console.log("Receiving answer")
                peerRef.current.setRemoteDescription(
                    new RTCSessionDescription(message.answer)
                )
            }

            if(message.iceCandidate){
                console.log("Receiving and adding ICE candidate")
                try {
                    await peerRef.current.addIceCandidate(message.iceCandidate)
                } catch (error) {
                    console.log("error receiving ice candidate", err)
                }
            }
        })
    })
  });

  const handleOffer = async (offer) => {
    console.log("Received offer, creating answer")
    peerRef.current = createPeer();
    
    await peerRef.current.setRemoteDescription(
        new RTCSessionDescription(offer)
    )

    userStream.current.getTracks().forEach((track)=> {
        peerRef.current.addTrack(track, userStream.current)
    })

    const answer = await peerRef.current.createAnswer();
    await peerRef.current.setLocalDescription(answer);

    webSocketRef.current.send(
        JSON.stringify({answer: peerRef.current.localDescription})
    )

  }

  const callUser = () => {
    console.log("Calling other user")
    peerRef.current = createPeer();

    userStream.current.getTracks().forEach((track) => {
        peerRef.current.addTrack(track, userStream.current)
    })
  }

  const createPeer = () => {
    console.log("Creating peer connection")
    const peer = new RTCPeerConnection({
        iceServers: [{urls: "stun:stun.l.google.com:19302"}]
    })

    peer.onnegotiationneeded = handleNegotiationNeeded;
    peer.onicecandidate = handleIceCandidateEvent;
    peer.ontrack = handleTrackEvent;

    return peer;
  }

  const handleNegotiationNeeded = async () => {
    console.log("creating offer")
    try {
        const myOffer = await peerRef.current.createOffer();
        await peerRef.current.setLocalDescription(myOffer)

        webSocketRef.current.send(JSON.stringify({offer: peerRef.current.localDescription}))
    } catch (error) {
        console.log(error)    
    }

  }

  const handleIceCandidateEvent = (e) => {
    console.log("found ice candidate")
    if(e.candidate){
        console.log(e.candidate)
        webSocketRef.current.send(JSON.stringify({iceCandidate: e.candidate}))
    }
  }

  const handleTrackEvent = (e) => {
    console.log("received tracks")
    partnerVideo.current.srcObject = e.streams[0]
  }

  return (
    <div>
        <video autoPlay controls={true} ref={userVideo}></video>
        <video autoPlay controls={true} ref={partnerVideo}></video>
        <RecordView />
        <button onClick={downloadScreenshot}>Download screenshot</button>
    </div>
  )
}
export default Room